﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestFromMDSN.Models;

namespace TestFromMDSN
{
    public class Collect
    {

        public List<User> users = new List<User>();
        public List<Conversation> conversations = new List<Conversation>();
        
       
        public Collect()
        {
            users.Add(new User { id = 1, name = "kilopo", lastname = "kilopo", city = "lviv" });
            users.Add(new User { id = 2, name = "marik", lastname = "kilopo", city = "sykhiv" });
            users.Add(new User { id = 3, name = "poliko", lastname = "kilopo", city = "lviv" });

            conversations.Add(new Conversation
            {
                id = 0,
                messages = new List<Message>(),
                members = new List<User>()
            });

            conversations.Add(new Conversation
            {
                id = 1,
                messages = new List<Message>(),
                members = new List<User>()
            });

            conversations[1].members.Add(new User { id = 1, name = "kilopo", lastname = "kilopo", city = "lviv" });
            conversations[1].members.Add(new User { id = 2, name = "marik", lastname = "kilopo", city = "sykhiv" });

            conversations[1].messages.Add(new Message { id = 1, author_id = 1, text = "Hello sir" });
            conversations[1].messages.Add(new Message { id = 2, author_id = 2, text = "Hi dude" });
            conversations[1].messages.Add(new Message { id = 3, author_id = 1, text = "wazaaap" });

        }

        public List<User> getUsers()
        {
            return users;
        }

        public List<Conversation> getConv(){

        
            return conversations;
        }

        public List<Message> getMessFromConv(int id)
        {
            return conversations[id].messages;
        }
    }
}