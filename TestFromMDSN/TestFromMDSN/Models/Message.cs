﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestFromMDSN.Models
{
    public class Message
    {
        public int id { get; set; }
        public int author_id { get; set; }
        public string text { get; set; }
        public DateTime date { get; set; }
    }
}