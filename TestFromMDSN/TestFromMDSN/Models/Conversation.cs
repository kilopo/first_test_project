﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestFromMDSN.Models;

namespace TestFromMDSN.Models
{
    public class Conversation
    {
        public int id { get; set; }
        public List<User> members { get; set; }
        public List<Message> messages { get; set; }
    }
}