﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TestFromMDSN.Models;

namespace TestFromMDSN.Controllers
{
    public class ConversationsController : ApiController
    {

        Collect conv = new Collect();



        public IEnumerable<Conversation> GetAllUsers()
        {
            
            return conv.getConv();
        }


        public IHttpActionResult GetUser(int Id)
        {
            var con = conv.getConv().FirstOrDefault((p) => p.id == Id);
            if (con == null)
            {
                return NotFound();
            }
            return Ok(con);
        }

    }
}
