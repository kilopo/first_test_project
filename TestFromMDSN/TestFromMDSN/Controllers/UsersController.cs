﻿using TestFromMDSN.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Collections;
using TestFromMDSN.Controllers;

namespace TestFromMDSN.Controllers
{
    public class UsersController : ApiController
    {



        Collect users = new Collect();
        
 
        public IEnumerable<User> GetAllUsers()
        {
            //return users;
            return users.getUsers();
        }

        public IHttpActionResult GetUser(int Id)
        {
            var user = users.getUsers().FirstOrDefault((p) => p.id == Id);
            if (user == null)
            {
                return NotFound();
            }
            return Ok(user);
        }
    }

    
}
